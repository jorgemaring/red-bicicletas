// // dos formas
// // la primera
// const {frutas, dinero} = require('./frutas');
// // la segunda
// // const obj = require('./frutas');
// // const frutas = obj.frutas;
// // const dinero = obj.dinero;

// const cowsay = require('cowsay');
// console.log(cowsay.say({
//     text: "Hola parceros",
//     e: 'oO',
//     T: 'U '
// }));

// frutas.forEach( fruta => console.count(fruta));
// console.log(dinero);
// HTTP

// const http = require('http');
// const server = http.createServer((request, response) => {
//     response.end('Estoy respondiendo a tu solicitud')
// });
// const puerto = 3000;
// server.listen(puerto, () => {
//     console.log('escuchando solicitudes');
// })

const express = require('express');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.use(express.static(__dirname + "/public"));

app.get('/', (req, res) => {
    res.render('index', {titulo: 'Mi titulo dinamico'});
});
app.get('/servicios', (req, res) => {
    res.render('servicios', {titulo: 'Servicios'});
});

app.use((req, res, next) => {
    res.status(404).render('404', {titulo: 'Error 404, página no encontrada'});
});
app.listen(port, () => {
    console.log('servidor corriendo en el puerto ', port)
});