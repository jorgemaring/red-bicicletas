var map = L.map('main_map').setView([10.4, -75.5], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
// L.marker([10.4, -75.49]).addTo(map)
//     .bindPopup('Por aquí vivo yo')
//     .openPopup();
    
// L.marker([10.41, -75.48]).addTo(map);
$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion).addTo(map)
                .bindPopup(bici.id);            
        });
    }
})