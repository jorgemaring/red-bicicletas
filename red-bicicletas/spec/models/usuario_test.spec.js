const mongoose = require('mongoose');
const User = require('../../models/usuario');
const Bicycle = require('../../models/bicicleta');
const reserva = require('../../models/reserva');
const dbTest = require('../db_test');

describe('Test user methods',()=>{
    beforeEach(async()=>{
        try{
            await dbTest.dbConnect();
        }catch(err){
            console.log(err);
        }
        
    })

    afterEach(async()=>{
        try{
            await reserva.deleteMany();
            await User.deleteMany();
            await Bicycle.deleteMany();
            console.log("Clear");
        }catch(err){
            console.log(err);
        }
        
    })

    describe('User.reserve',()=>{
        it('must create a user reservation',async()=>{
            try{
                const user = new User({name:"Juan"});
                await user.save();
                const users = await User.find();
                console.log(users);
                expect(users.length).toBe(1);
                const bicy = Bicycle.createInstance(5,'Black','Mountain',[6.765439, -75.888765]);
                await Bicycle.add(bicy);
                console.log(bicy);
                expect(Bicycle.allBicycle.length).toBe(1);

                const today = new Date();
                const tomorrow = new Date();

                tomorrow.setDate(today.getDate()+1);

                await users[0].reserve(bicy._id,today,tomorrow);

                const reservations = await reserva.find().populate('bicycle').populate('user').exec();
                console.log(reservations);
                expect(reservations.length).toBe(1);
                expect(reservations[0].user.name).toBe(user.name);
                expect(reservations[0].user.id).toBe(users[0].id);
                expect(reservations[0].reservationDays()).toBe(2);
                

            }catch(err){
                console.log(err);
            }
        })

    })
})
