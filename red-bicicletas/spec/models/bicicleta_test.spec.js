var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');
describe('Testing Bicicletas', function(){
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database:');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        })
    })
    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance('1', 'verde', 'urbana', [-2,-3]);
            expect(bici.code).toBe('1');
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-2);
            expect(bici.ubicacion[1]).toEqual(-3);
        })
    });
    
    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(Bicicleta.allBicis.length).toBe(0);
                done();                
            });
        });
    });
    describe('Bicicleta.add', () => {
        it('agregamos una bicicleta', (done) =>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta('1', 'rojo', 'urbana');
            Bicicleta.add(a, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(a.code);
                })
            });
            expect(Bicicleta.allBicis.length).toBe(1);
            expect(Bicicleta.allBicis[0]).toBe(a);
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('buscamos una bicicleta', (done) =>{            
            Bicicleta.allBicis(function(err, bicis){
                expect(Bicicleta.allBicis.length).toBe(0);
                var a = new Bicicleta('1', 'rojo', 'urbana');
                Bicicleta.add(a, function(err, newBici){
                    if(err) console.log(err);
                    var b = new Bicicleta('2', 'negra', 'ruta');
                    Bicicleta.add(a, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode('1', function(error, targetBici){
                            expect(targetBici.code).toBe(a.code);
                            expect(targetBici.color).toBe(a.color);
                            expect(targetBici.modelo).toBe(a.bici);
                            done();
                        });
                    });
                });
            });
            expect(Bicicleta.allBicis.length).toBe(1);
            expect(Bicicleta.allBicis[0]).toBe(a);
        });
    });
});
// beforeEach(() => {
//     Bicicleta.allBicis = [];
// });
// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });
// describe('Bicicleta.add', () => {
//     it('agregamos una bicicleta', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta('1', 'rojo', 'urbana', [10.415, -75.483]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });
// describe('Bicicleta.findById', () => {
//     it('Debe devolver la bici con id 1', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici = new Bicicleta('1', 'verde', 'urbana');
//         var aBici2 = new Bicicleta('2', 'amarillo', 'ruta');
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);
//         var targetBici = Bicicleta.findById('1');
//         expect(targetBici.id).toBe('1');
//         expect(targetBici.color).toBe(aBici.color);
//         expect(targetBici.modelo).toBe(aBici.modelo);
//     });
// });
// describe('Bicicleta.removeById', () => {
//     it('eliminamos una bicicleta', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta('1', 'rojo', 'urbana', [10.415, -75.483]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         Bicicleta.removeById(a.id);
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });