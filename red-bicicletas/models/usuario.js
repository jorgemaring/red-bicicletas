var mongoose = require('mongoose');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const mailer = require('../mailer/mailer');
let Token = require('../models/token');
const saltRounds = 10;
const uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;
const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        // required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        // validate: [validateEmail, 'Por favor, ingresa un email válido'],
        // match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String

});
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'});
usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reserva = function (bicicletaId, desde, hasta, cb) {  
    let reserva = new Reserva({
        usuario: this._id,
        bicicleta: bicicletaId, 
        desde: desde,
        hasta: hasta
    });

    reserva.save(cb);
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    console.log('self'+self);
    console.log("this password"+condition.password);
    self.findOne({
        $or: [{
            'googleId': condition.id
        },
        {
            'email': condition.emails[0].value
        }]
    }, (err, result) => {
        if (result) {
            console.log(result + ' Resil;sadmas');
            callback(err, result);
        } else {
            console.log("----condition-----------");
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            console.log(values.email);
            values.name = condition.displayName || "Not name";
            values.verified = true;
            values.password = bcrypt.hashSync(condition.id, saltRound);
            console.log('---------------values-----------------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err) };
                callback(err, result)
            })
        }
    }
    )
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    console.log('self'+self);
    console.log("this password"+condition.password);
    self.findOne({
        $or: [{
            'facebookId': condition.id
        },
        {
            'email': condition.emails[0].value
        }]
    }, (err, result) => {
        if (result) {
            console.log(result + ' Resil;sadmas');
            callback(err, result);
        } else {
            console.log("----condition-----------");
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            console.log(values.email);
            values.name = condition.displayName || "Not name";
            values.verified = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('---------------values-----------------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err) };
                callback(err, result)
            })
        }
    }
    )
};

usuarioSchema.methods.sendEmail = function(cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex')
    });
    const emailDestination = this.email;
    
    token.save(function(err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@red-bicicletas.com',
            to: emailDestination,
            subject: 'Account Verification',
            text: 'Hola,\n\n' + 'Por favor, verifica tu cuenta, click en el siguiente enlace:\n\n' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) {
                return console.log(err.message);
            }

            console.log('Se ha enviado un email a ' + emailDestination + '.');
        });
    });
};

usuarioSchema.methods.booking = function (bicycleId, from, to, cb) {  
    let booking = new Reserva({
        user: this._id,
        bicycle: bicycleId, 
        from: from,
        to: to
    });
    booking.save(cb);
};


usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err) {
        if (err) { return cb(err); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link:\n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return cb(err); }
            console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
        });

        cb(null);
    });
};



module.exports = mongoose.model('Usuario', usuarioSchema);