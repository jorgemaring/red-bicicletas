var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bicicletaSchema = new mongoose.Schema({
    code: String,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type:'2dsphere', sparse: true}
    }
});
bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}
bicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
}
bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
}
bicicletaSchema.statics.add = function(aBici, cb) {
    this.create(aBici, cb);
}
bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({code: aCode}, cb);
}
bicicletaSchema.statics.add = function(aCode, cb) {
     return this.deleteOne({code: aCode}, cb);
}
module.exports = mongoose.model('Bicicleta', bicicletaSchema);
// var Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function (){
//     return 'id: +' + this.id + " | color: " + this.color;
// }
// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici) {
//     Bicicleta.allBicis.push(aBici);
// }
// Bicicleta.removeById = function(aBiciId) {
//     // Bicicleta.allBicis.forEach( (bici, index) => {
//     //     if (bici.id === aBiciId) {
//     //         Bicicleta.allBicis.splice(index, 1);
//     //     }
//     // });
//     for (let index = 0; index < Bicicleta.allBicis.length; index++) {
//         if( Bicicleta.allBicis[index].id === aBiciId) {
//             Bicicleta.allBicis.splice(index,1);
//             break;
//         }
        
//     }
// }
// Bicicleta.findById = function(aBiciId) {
//     var aBici = Bicicleta.allBicis.find(x => x.id === aBiciId);
//     if ( aBici)
//         return aBici;
//     else   
//         throw new Error(`No existe una bicicleta con el id ${aBiciId}`);

// }
// // var a = new Bicicleta('1', 'rojo', 'urbana', [10.415, -75.483]);
// // var b = new Bicicleta('2', 'azul', 'urbana', [10.418, -75.481]);
// // Bicicleta.add(a);
// // Bicicleta.add(b);
// module.exports = Bicicleta;